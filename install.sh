echo "##################################################"
echo "# Bienvenue dans l'installation bootstrap du VPS #"
echo "##################################################"

echo "###"
echo "# Mise à jour d'Ubuntu "
echo

sudo apt update
sudo apt upgrade -y

echo "###"
echo "Installation de zsh"
echo

sudo apt install zsh -y

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
sed -i -re 's/^(ZSH_THEME=").*".*$/\1xiong-chiamiov-plus"/' .zshrc
sed -i -re 's/^(plugins=\().*\).*$/\1git ubuntu per-directory-history)/' .zshrc

echo "###"
echo "Installation de tmux"
echo

sudo apt install tmux
curl -o ~/.tmux.conf https://gitlab.com/stair-utils/Init-Vps/-/raw/master/tmux/tmux.conf
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
sed -i -re 's/^(plugins=\(.*)\).*$/\1 tmux)/' .zshrc
sed -i -re 's/^(plugins.*)$/\1\n\nexport ZSH_TMUX_AUTOSTART=true\nexport ZSH_TMUX_AUTOSTART_ONCE=false\nexport ZSH_TMUX_AUTOCONNECT=false\n/' .zshrc

echo "###"
echo "Installation de asdf"
echo

if [ ! -d ~/.asdf ]
then
    git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.8.0
fi
sed -i -re 's/^(plugins=\(.*)\).*$/\1 asdf)/' .zshrc

echo "###"
echo "Installation de Erlang Elixir"
echo
sudo apt install -y unzip build-essential autoconf libssl-dev libncurses5-dev
asdf install erlang latest
asdf global erlang 23.1.5
asdf install elixir latest

